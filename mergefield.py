# encoding=utf-8
import sys
import argparse
from jinja2 import Environment, PackageLoader
import io


env = Environment(
    loader=PackageLoader('mergefield', 'templates')
)

ap = argparse.ArgumentParser(description="生成word模板域字段")

ap.add_argument('cname', help='生成的类名')


ns = ap.parse_args()

field_template = env.get_template('mergefield.jinja')

result_list = []
if __name__ == '__main__':

    for line in sys.stdin:
        field_list = line.strip().split('\t')
        index = 0
        dic = {'field': field_list[2], 'desc': field_list[0], 'cname': ns.cname}

        result = field_template.render(dic)

        result_list.append(result)

    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

    sys.stdout.write("\n".join(result_list))

