from pypinyin import Style, pinyin
import sys


def getpy(string):
    """
    :param string: 要获取大写简拼的中文字符串
    :return: 转换为大写简拼字符串
    """
    ll2s = pinyin(string, Style.FIRST_LETTER)
    pinyin_str = ''
    for cl in ll2s:
        for c in cl:
            pinyin_str += c.__str__()
    return pinyin_str.upper()


if __name__ == '__main__':
    [sys.stdout.write(l.rstrip() + '\t' + getpy(l.rstrip()) + '\n') for l in sys.stdin]
