import sys
import argparse
from jinja2 import Environment, PackageLoader

ap = argparse.ArgumentParser(description="生成建表语句")

ap.add_argument('t_name', nargs='?', help='表名')

ns = ap.parse_args()

env = Environment(
    loader=PackageLoader('create_table', 'templates')
)

field_template = env.get_template('create_table.jinja')
column_list = []
if __name__ == '__main__':
    for line in sys.stdin:
        column_attr = line.strip('\n') \
            .replace('VARCHAR', 'VARCHAR2(1024)') \
            .replace('NUMBER', 'NUMBER(20,4)') \
            .replace('INTEGER', 'NUMBER(10)') \
            .split('\t')
        column_dic = {'comment': column_attr[0], 'name': column_attr[1], 'java': column_attr[2], 'type': column_attr[3]}
        column_list.append(column_dic)

    dic = {'cols': column_list, 't_name': ns.t_name}

    result = field_template.render(dic)

    sys.stdout.write(result)
