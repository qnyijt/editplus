import sys
import argparse
from jinja2 import Environment, PackageLoader

env = Environment(
    loader=PackageLoader('temp_common', 'templates')
)

ap = argparse.ArgumentParser(description="通用简单模板生成器")

ap.add_argument('tempName', help='模板名称，不需要文件扩展名')
ap.add_argument('sortArg', nargs='*', help='同一行的参数从左至右的名称')

ns = ap.parse_args()

field_template = env.get_template(ns.tempName + '.jinja')

result_list = []
for line in sys.stdin:
    field_list = line.strip().split('\t')
    index = 0
    dic = {}
    for arg in ns.sortArg:
        dic[arg] = field_list[index]
        index = index + 1

    result = field_template.render(dic)

    result_list.append(result)

sys.stdout.write("\n".join(result_list))
