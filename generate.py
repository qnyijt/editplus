import sys

from jinja2 import Environment, PackageLoader
env = Environment(
    loader=PackageLoader('generate', 'templates')
)

field_template = env.get_template('field.jinja')
result_list = []
for line in sys.stdin:
    field_list = line.strip('\n') \
        .replace('INTEGER', 'int') \
        .replace('VARCHAR2', 'string') \
        .replace('VARCHAR', 'string') \
        .replace('NUMBER(20,4)', 'long') \
        .replace('NUMBER', 'long') \
        .replace('userID', 'czr') \
        .replace('userId', 'czr') \
        .split('\t')

    result_list.append(field_template
                       .render(source=field_list[1],
                               java=field_list[2],
                               type=field_list[3],
                               desc=field_list[0]))

sys.stdout.write(',\n'.join(result_list))
