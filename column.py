import sys

from jinja2 import Environment, PackageLoader


def get_attr_dict(field_type):
    """
    :param field_type: 要解析的数据类型
    :return: 根据数据类型解析出的参数字典
    """
    field_type = field_type.replace('，', ',').replace('（', '(').replace('）', ')').upper()

    check_has_scale = field_type.find(',')
    scale = 0 if check_has_scale == -1 else field_type[check_has_scale + 1:field_type.find(')')]

    if field_type.__contains__('VARCHAR'):
        typ = 0
        biz_type = 100
        length = 1024
    elif field_type.__contains__('NUMBER'):
        typ = 1
        biz_type = 200
        length = 14
        scale = 4
    elif field_type.__contains__('DATE'):
        typ = 13
        biz_type = 300
        length = 8
    elif field_type.__contains__('CLOB'):
        typ = 0
        biz_type = 200
        length = 3000
    else:
        typ = 0
        biz_type = 100
        length = 1024

    check_has_length = field_type.find('(')
    length = length if check_has_length == -1 else field_type[check_has_length + 1:field_type.find(')')].split(',')[0]

    attr_dict = {'type': typ, 'biz_type': biz_type, 'length': length, 'scale': scale}

    return attr_dict


if __name__ == '__main__':
    """
    客户姓名	KHXM	custName	VARCHAR
    默认生成的数字型无精度，如需要精度需手动在NUMBER后加上',$scale)'
    """

    env = Environment(
        loader=PackageLoader('column', 'templates')
    )

    field_template = env.get_template('column.jinja')

    lists = []
    for l_input in sys.stdin:
        attr_list = l_input.split('\t')
        attr_dic = {'desc': attr_list[0], 'name': attr_list[1]}
        dic = get_attr_dict(attr_list[3])
        lists.append(field_template.render({**attr_dic, **dic}))

    sys.stdout.write("\n" + "\n".join(lists))
