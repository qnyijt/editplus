from jianpin import getpy
from translate import translate
from translate import load_dic

import sys

if __name__ == '__main__':
    result_list = []
    for line in sys.stdin:
        result_list.append(line.strip() + '\t' + getpy(line.strip()) + '\t' + translate(load_dic(), line.strip(), True))
    sys.stdout.write("\n".join(result_list))
