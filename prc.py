import sys

from jinja2 import Environment, PackageLoader

env = Environment(
    loader=PackageLoader('prc', 'templates')
)

field_template = env.get_template('homepage_query_prc.jinja')


i = 0
for line in sys.stdin:
    field_list = line.strip().split('\t')
    result = field_template.render(comment=field_list[0],
                                   prc=field_list[1])

    with open('./output/' + field_list[1] + '.prc', 'w') as prc_file:
        prc_file.write(result)
