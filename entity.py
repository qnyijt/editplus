import sys
import uuid
from jinja2 import Environment, PackageLoader

env = Environment(
    loader=PackageLoader('entity', 'templates')
)

field_template = env.get_template('entity.jinja')

i = 0
for l in sys.stdin:
    field_list = l.strip().split('\t')
    result = field_template.render(desc=field_list[0],
                                   table=field_list[1],
                                   id=uuid.uuid4())

    with open('./output/' + field_list[1] + '.xml', 'w') as prc_file:
        prc_file.write(result)
