import sys

from jinja2 import Environment, PackageLoader
env = Environment(
    loader=PackageLoader('update', 'templates')
)
field_template = env.get_template('update.jinja')

result_list = []
for line in sys.stdin:
    field_list = line.strip('\n') \
        .replace('INTEGER', 'int') \
        .replace('VARCHAR', 'string') \
        .replace('NUMBER', 'long') \
        .replace('userID', 'czr') \
        .split('\t')

    result_list.append(field_template
                       .render(comment=field_list[0],
                               p1=field_list[1],
                               p2=field_list[1].split('_')[1]))

sys.stdout.write('\n'.join(result_list))
