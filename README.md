## 简单说明

这个仓库保存了一些python脚本，主要用来和（可以自定义工具的）编辑器结合，简化一些输入工作。

**[url_decode.py](./url_decode.py)**

将文本中进行了url编码的部分原地解码成汉字

**[jianpin.py](./jianpin.py)**

将文本中的汉字原地替换为拼音简拼大写

## jinja2简单用法

[jinja2官方api地址](https://jinja.palletsprojects.com/en/2.11.x/api/)

[模板设计文档](https://jinja.palletsprojects.com/en/2.11.x/templates/)

简单模板：

```
{
    "sourceField": "{{source}}",
    "javaFiels": "{{java}}",
    "fielsType": "{{type}}",
    "fielsDescription": "{{desc}}"
}
```

简单使用示例：

```python
# 引入包
from jinja2 import Environment, PackageLoader

# 创建Environment对象，PackageLoader的第一个参数为应用名称，简单脚本时为文件名；第二个参数是模板存放路径名。
env = Environment(
    loader=PackageLoader('generate', 'templates')
)

# 从文件中获取模板
field_template = env.get_template('field.jinja')

# 使用数据渲染
field_template.render(source=field_list[1],
                      java=field_list[2],
                      type=field_list[3],
                      desc=field_list[0])
```

