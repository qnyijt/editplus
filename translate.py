import collections
import sys


def check_none_chinese(string):
    """
    检查字符串中是否含有中文
    :param string: 要检查的字符串
    :return: 不含中文返回true，含有中文返回false
    """
    for ch in string:
        if u'\u4e00' <= ch <= u'\u9fff':
            return False

    return True


def translate(order_dic, item, is_mark_fail):
    """
    根据排序字典将词条转化为英文
    :param is_mark_fail: 有未翻译的汉字时是否标注
    :param order_dic: 排序字典
    :param item: 要翻译的词条
    :return: 翻译后的结果，默认为小写字母，下划线隔开
    """
    for c, e in order_dic.items():
        if item.find(c) > -1 and not check_none_chinese(item):
            item = item.replace(c, e)
    # 去除前导_
    if item[:1] == '_':
        item = item[1:]
    # 如果失败，使用双星号进行标注
    if is_mark_fail:
        item = item if check_none_chinese(item) else "**" + item + "**"

    return item


def load_dic():
    """
    将c2e.txt读取到排序字典中
    :return: 读取出的排序字典
    """
    c2e_dic = collections.OrderedDict()
    with open("./config/c2e.txt", "r+", encoding='utf-8') as dic_file:
        for line in dic_file:
            field_list = line.strip().split("\t")
            c2e_dic[field_list[0]] = field_list[1]

    return c2e_dic


if __name__ == '__main__':
    for input_line in sys.stdin:
        eng = translate(load_dic(), input_line.strip(), True)

        sys.stdout.write(input_line.strip() + "\t" + eng + "\n")
