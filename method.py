import sys

from jinja2 import Environment, PackageLoader


if __name__ == '__main__':
    env = Environment(
        loader=PackageLoader('method', 'templates')
    )

    field_template = env.get_template('method.jinja')

    result_list = []
    for l in sys.stdin:
        field_list = l.strip().split('\t')
        dic = {'desc': field_list[0], 'prc': field_list[1], 'method': field_list[2]}
        result = field_template.render(dic)
        result_list.append(result)

    sys.stdout.write(",\n".join(result_list))
