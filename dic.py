import sys
import argparse
from jinja2 import Environment, PackageLoader


ap = argparse.ArgumentParser(description="生成查询字典语句")

ap.add_argument('dicCode', nargs=1, help='字典代码')

ns = ap.parse_args()
dic_code = ns.dicCode[0]
env = Environment(
    loader=PackageLoader('dic', 'templates')
)

field_template = env.get_template('dic.jinja')
result_list = []
for line in sys.stdin:
    field_list = line.strip('\n') \
        .split('\t')

    result_list.append(field_template
                       .render(dic_code=dic_code,
                               field=field_list[1],
                               alias=field_list[2],
                               comment=field_list[0]))

sys.stdout.write('\n'.join(result_list))
