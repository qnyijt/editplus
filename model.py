# encoding=utf-8
import sys
import argparse
from jinja2 import Environment, PackageLoader
import io

env = Environment(
    loader=PackageLoader('model', 'templates')
)

ap = argparse.ArgumentParser(description="生成model")

ap.add_argument('cname', help='生成的类名')
ap.add_argument('mname')

ns = ap.parse_args()

field_template = env.get_template('model.jinja')
stat_template = env.get_template('statement.jinja')

context = {'cname': ns.cname}
context = {'mname': ns.mname}

fl = []
if __name__ == '__main__':

    for line in sys.stdin:
        field_list = line.strip().split('\t')
        field_dic = {'comment': field_list[0], 'name': field_list[2],
                     'upname': "".join(field_list[2][:1].upper() + field_list[2][1:])}
        fl.append(field_dic)

    context['fs'] = fl

    result = field_template.render(context)
    print(result)
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

    with open('C:\\Users\\yajie\\apex\\code\\poss-ams\\ams-client-org-pos-hotchpotch\\src\\main\\java\\com\\apexsoft'
              '\\crm\\project\\model\\ApprRepExport' + ns.mname + 'Model.java', 'w', encoding='utf-8') as model_file:
        model_file.write(result)

    dic = {'cname': ns.cname, 'lname': "".join(ns.cname[:1].lower() + ns.cname[1:]), 'mname': ns.mname}
    sys.stdout.write(stat_template.render(dic))
