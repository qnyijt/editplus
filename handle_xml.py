import sys
import argparse
import xml.etree.cElementTree as cET

ap = argparse.ArgumentParser(description="xml处理参数")

ap.add_argument('extractField', nargs='*', help='要提取的字段')
ap.add_argument('-s', nargs='?', default='\n', help='提取字段的分隔符')

ns = ap.parse_args()

s = sys.stdin.read()
root = cET.fromstring(s)

result_list = []

for content in root.iter('*'):
    if ns.extractField.__contains__(content.tag):
        result_list.append(content.text)

sys.stdout.write(ns.s.join(result_list))
